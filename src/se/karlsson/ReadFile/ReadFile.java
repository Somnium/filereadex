package se.karlsson.ReadFile;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class ReadFile {
	public static final String FILE_PATH = "myfile.txt";

	public static void main(String[] args) {
		File f = new File("/myFile.txt");
		// IF METHOD FOR CREATING NEW FILE IF NOT EXISTING
		if (!f.exists()) {
			try {
				f.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		myReader reader = new myReader();
		reader.readMyFile();

		ArrayList<String> found = reader.findContact("b");

		for (String s : found) {
			System.out.println(s);
		}
		System.out.println(reader.findNumberFromContact("Alba"));

	}
}
