package se.karlsson.ReadFile;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map.Entry;
import java.util.TreeMap;

public class myReader {

	TreeMap<String, String> contactList = new TreeMap<>();

	public void readMyFile() {

		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(ReadFile.FILE_PATH));
			String line;
			while ((line = br.readLine()) != null) {
				String[] contact = line.split(";");
				contactList.put(contact[0], contact[1]);

			}
			// h�mta och anv�nda ett v�rde fr�n en annan klass
			// close
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

	public ArrayList<String> findContact(String contact) {
		ArrayList<String> found = new ArrayList<>();
		for (Entry<String, String> entry : contactList.entrySet()) {
			if (entry.getValue().contentEquals(contact)) {
				found.add(entry.getValue());
			}
		}

		return found;

	}

	public String findNumberFromContact(String contact) {
		String found = null;
		for (Entry<String, String> entry : contactList.entrySet()) {
			if (entry.getValue() == contact) {
				found = entry.getKey();
				break;
			}
		}
		return found != null ? found : "Not found";
	}
}
